@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4>Nama: {{ $post->nama }}</h4>
        <h4>Umur: {{ $post->umur }}</h4>
        <h4>Bio: {{ $post->bio }}</h4>
    </div>
@endsection

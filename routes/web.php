<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/master', function () {
//     return view('adminlte.master');
// });

// Route::get('/table', function () {
//     return view('table.table');
// });

// Route::get('/data-tables', function () {
//     return view('table.data-tables');
// });

// Route dengan cara manual
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast', 'CastController@index')->name('cast.index');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

// Route::resource('posting', 'PostingController');

// menggunakan Route resource controller
Route::resource('cast', 'CastController');

// middleware di panggil di semua route
// Route::resource('cast', 'CastController')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

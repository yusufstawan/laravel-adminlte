<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->except('index');
        $this->middleware('auth')->only('create', 'edit', 'update', 'store');
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);
        // Query Builder
        // $query = DB::table('cast')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        // Eloquent ORM
        // $cast = new Cast;
        // $cast->nama = $request['nama'];
        // $cast->umur = $request['umur'];
        // $cast->bio = $request['bio'];
        // $cast->save();

        // Eloquent ORM with Mass Assignment
        $cast = Cast::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Data Cast berhasil ditambahkan');
    }

    public function index()
    {
        // Query Builder
        // $posts = DB::table('cast')->get();
        // dd($posts);

        // Eloquent ORM
        $posts = Cast::all();

        return view('post.index', compact('posts'));
    }

    public function show($id)
    {
        // Query Builder
        // $post = DB::table('cast')->where('id', $id)->first();
        // dd($post);

        // Eloquent ORM
        $post = Cast::find($id);
        return view('post.show', compact('post'));
    }

    public function edit($id)
    {
        // Query Builder
        // $post = DB::table('cast')->where('id', $id)->first();
        // dd($post);

        // Eloquent ORM
        $post = Cast::find($id);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|numeric',
            'bio' => 'required',
        ]);

        // Query Builder
        // $query = DB::table('cast')->where('id', $id)->update([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        // Eloquent ORM
        $update = Cast::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('success', 'Data Cast berhasil diubah');
    }

    public function destroy($id)
    {
        // Query Builder
        // $query = DB::table('cast')->where('id', $id)->delete();

        // Eloquent ORM
        Cast::destroy($id);
        return redirect('/cast')->with('success', 'Data Cast berhasil dihapus');
    }
}
